# Nextcloud Notes for Android
An android client for [Nextcloud Notes App](https://github.com/nextcloud/notes/).

### How to install nextcloud notes app in /e/OS?

1. Go to https://gitlab.e.foundation/dapsi-e2ee/nextcloud-notes/-/releases/v1.0.0-E2EE and download `app-dev-release.apk` in the mobile device
2. After downloading open the apk file and it will start installing the application
3. Note that, [Open keychain](https://f-droid.org/packages/org.sufficientlysecure.keychain/) app and [Nextcloud](https://f-droid.org/en/packages/com.nextcloud.client/) app is also needed to be installed/enabled for this app to work
4. Sign in to your nextcloud account and import your pgp keys in open keychain
5. Open notes app and select the desired pgp key
6. Encryption and decryption will start working

### How does notes app in /e/OS work?

Notes app in /e/OS use OpenKeychain to encrypt and decrypt Note files. All note files between web and mobile are synced, so user has to import same key pair that was originally created in mailvelope to be able to decrypt files that were created in web.

### Improvements:

* Improve UI for showing encrypted files
* Settings for setup encryption keys

### :link: Requirements
  * [Nextcloud](https://nextcloud.com/) instance running
  * [Nextcloud Android](https://github.com/nextcloud/android) app installed (≥ 3.9.0)
  * [Nextcloud Notes](https://github.com/nextcloud/notes) app enabled


### :notebook: License
This project is licensed under the [GNU GENERAL PUBLIC LICENSE](/LICENSE).
