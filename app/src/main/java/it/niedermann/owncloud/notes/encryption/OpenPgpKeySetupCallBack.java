package it.niedermann.owncloud.notes.encryption;

import android.app.PendingIntent;
import android.content.IntentSender;

import androidx.annotation.Nullable;

public interface OpenPgpKeySetupCallBack {

    void startPendingIntentForResult(PendingIntent pendingIntent, int requestCode) throws IntentSender.SendIntentException;

    void saveKeyId(long key, EncryptionState state);

    void operationFailed(String message, EncryptionState state);

    int getNewRequestCode();

    @Nullable
    EncryptionState getStateByRequestCode(int requestCode);
}
