package it.niedermann.owncloud.notes.encryption;

import androidx.annotation.NonNull;
import androidx.annotation.UiThread;

public interface NoteEncryptionCallBack {

    boolean isEncryptionAlreadySetup();

    @UiThread
    void restartEncryptionSetup(@NonNull EncryptionCallBack callBack);

    @UiThread
    void startEncryptionSetup(@NonNull EncryptionCallBack callBack);

    @UiThread
    void getNoteContent(@NonNull String content, @NonNull EncryptionCallBack callBack);

    @UiThread
    void encryptNoteContent(@NonNull String content, @NonNull EncryptionCallBack callBack);
}
