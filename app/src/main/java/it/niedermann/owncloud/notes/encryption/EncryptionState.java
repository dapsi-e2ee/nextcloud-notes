package it.niedermann.owncloud.notes.encryption;

class EncryptionState {

    private final Type type;
    private final String providedContent;
    private final EncryptionCallBack callBack;
    private int currentRequestCode = 0;
    private String resultedContent = null;
    private boolean onKeySetupState = false;

    EncryptionState(Type type, String providedContent, EncryptionCallBack callBack) {
        this.type = type;
        this.providedContent = providedContent;
        this.callBack = callBack;
    }

    Type getType() {
        return type;
    }

    String getProvidedContent() {
        return providedContent;
    }

    public EncryptionCallBack getCallBack() {
        return callBack;
    }

    int getCurrentRequestCode() {
        return currentRequestCode;
    }

    EncryptionState setCurrentRequestCode(int currentRequestCode) {
        this.currentRequestCode = currentRequestCode;
        return this;
    }

    public String getResultedContent() {
        return resultedContent;
    }

    public void setResultedContent(String resultedContent) {
        this.resultedContent = resultedContent;
    }

    public boolean isOnKeySetupState() {
        return onKeySetupState;
    }

    public void setOnKeySetupState(boolean onKeySetupState) {
        this.onKeySetupState = onKeySetupState;
    }

    enum Type {
        ENCRYPT,
        DECRYPT,
        SET_UP
    }
}
