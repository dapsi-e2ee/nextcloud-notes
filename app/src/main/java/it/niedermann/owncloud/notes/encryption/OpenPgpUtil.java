package it.niedermann.owncloud.notes.encryption;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;

import androidx.annotation.Nullable;

import org.openintents.openpgp.util.OpenPgpApi;

import java.util.ArrayList;
import java.util.List;

public class OpenPgpUtil {

    @Nullable
    public static String getOpenPgpProviderPackage(Context context) {
        final List<String> openPgpProviderPackages = getOpenPgpProviderPackages(context);
        if (openPgpProviderPackages.size() > 0) {
            return openPgpProviderPackages.get(0);
        }
        return null;
    }

    private static List<String> getOpenPgpProviderPackages(Context context) {
        ArrayList<String> result = new ArrayList<>();
        Intent intent = new Intent(OpenPgpApi.SERVICE_INTENT_2);
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentServices(intent, 0);
        if (resInfo != null) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.serviceInfo == null) {
                    continue;
                }

                result.add(resolveInfo.serviceInfo.packageName);
            }
        }
        return result;
    }
}
