package it.niedermann.owncloud.notes.encryption;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;

import com.nextcloud.android.sso.exceptions.NextcloudFilesAppAccountNotFoundException;
import com.nextcloud.android.sso.exceptions.NoCurrentAccountSelectedException;
import com.nextcloud.android.sso.helper.SingleAccountHelper;
import com.nextcloud.android.sso.model.SingleSignOnAccount;

import java.util.HashMap;
import java.util.Map;

import it.niedermann.owncloud.notes.R;
import it.niedermann.owncloud.notes.shared.util.SupportUtil;

public abstract class EncryptionSupportActivity extends AppCompatActivity implements OpenPgpSetupCallBack, NoteEncryptionCallBack {

    private static final String TAG = EncryptionSupportActivity.class.getSimpleName();

    private EncryptionSetUpHelper encryptionSetUpHelper = null;
    private int requestCode = 9912;
    private Map<String, EncryptionState> runningEncryptions;

    @Override
    protected void onDestroy() {
        if (encryptionSetUpHelper != null) {
            encryptionSetUpHelper.onDestroy();
        }

        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (encryptionSetUpHelper != null) {
            encryptionSetUpHelper.handleOnActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public int getNewRequestCode() {
        return requestCode++;
    }

    @Nullable
    @Override
    public EncryptionState getStateByRequestCode(int requestCode) {
        EncryptionState result = null;

        for (String providedContent : getRunningEncryptions().keySet()) {
            var state = getRunningEncryptions().get(providedContent);
            if (state != null && state.getCurrentRequestCode() == requestCode) {
                result = state;
                break;
            }
        }

        return result;
    }

    @Override
    public boolean isEncryptionAlreadySetup() {
        initEncryptionSetUpHelperIfNeeded();

        if (encryptionSetUpHelper == null) {
            return false;
        }

        return encryptionSetUpHelper.getCurrentStatus() == EncryptionStatus.READY;
    }

    @Override
    @UiThread
    public void restartEncryptionSetup(@NonNull EncryptionCallBack callBack) {
        initEncryptionSetUpHelperIfNeeded();

        if (encryptionSetUpHelper == null) {
            return;
        }

        encryptionSetUpHelper.clearSetup();
        startEncryptionSetup(callBack);
    }

    @Override
    @UiThread
    public void startEncryptionSetup(@NonNull EncryptionCallBack callBack) {
        startEncryption("setup encryption " + TAG, callBack, EncryptionState.Type.SET_UP);
    }

    // return note content as plain text. If the content is encrypted, decrypt that first.
    @Override
    @UiThread
    public void getNoteContent(@NonNull String content, @NonNull EncryptionCallBack callBack) {
        if (handlePlainContent(content, callBack)) {
            return;
        }

        startEncryption(content, callBack, EncryptionState.Type.DECRYPT);
    }

    @Override
    @UiThread
    public void encryptNoteContent(@NonNull String content, @NonNull EncryptionCallBack callBack) {
        startEncryption(content, callBack, EncryptionState.Type.ENCRYPT);
    }



    private void startEncryption(@NonNull String content, @NonNull EncryptionCallBack callBack, @NonNull EncryptionState.Type type) {
        if (checkOperationAlreadyRunning(content, callBack) || !checkEncryptionPossible(content, callBack)) {
            return;
        }

        var state = setUpNewState(content, callBack, type);

        switch (encryptionSetUpHelper.getCurrentStatus()) {
            case SETUP_REQUIRED:
                startEncryptionSetup(state);
                break;
            case READY:
                startOperation(state);
                break;
        }
    }

    private EncryptionState setUpNewState(@NonNull String content, @NonNull EncryptionCallBack callBack, @NonNull EncryptionState.Type type) {
        var state = new EncryptionState(type, content, callBack);
        getRunningEncryptions().put(content, state);
        return state;
    }

    private void removeState(@NonNull EncryptionState state) {
        getRunningEncryptions().remove(state.getProvidedContent());
    }

    private boolean checkOperationAlreadyRunning(@NonNull String content, @NonNull EncryptionCallBack callBack) {
        if (getRunningEncryptions().containsKey(content)) {
            callBack.contentReady(false, content);
            return true;
        }
        return false;
    }

    private void startEncryptionSetup(EncryptionState state) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.encryption_setup_message)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    startOperation(state);
                    dialog.dismiss();
                })/*.setNegativeButton(R.string.no, (dialog, which) -> {
                    passResultViaCallBack(false, state);
                    dialog.dismiss();
                })*/
                .setCancelable(false)
                .show();
    }

    private void startOperation(EncryptionState state) {
        if (state != null) {
            encryptionSetUpHelper.operate(state);
        }
    }

    private boolean checkEncryptionPossible(@NonNull String content, @NonNull EncryptionCallBack callBack) {
        initEncryptionSetUpHelperIfNeeded();
        if (encryptionSetUpHelper == null) {
            Toast.makeText(this, R.string.encryption_init_failed_for_no_account_selected, Toast.LENGTH_SHORT).show();
            callBack.contentReady(false, content);
            return false;
        }

        return true;
    }

    private boolean handlePlainContent(@NonNull String content, @NonNull EncryptionCallBack callBack) {
        if (!SupportUtil.isEncryptedText(content)) {
            callBack.contentReady(true, content);
            return true;
        }

        return false;
    }

    private void initEncryptionSetUpHelperIfNeeded() {
        if (encryptionSetUpHelper == null) {
            try {
                SingleSignOnAccount account = SingleAccountHelper.getCurrentSingleSignOnAccount(this);

                if (account == null) {
                    return;
                }

                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.N) {
                    return;
                }

                String name = SupportUtil.getUserName(account.name);
                if (TextUtils.isEmpty(name)) {
                    return;
                }

                encryptionSetUpHelper = new EncryptionSetUpHelperImpl(this, name, this);
            } catch (NextcloudFilesAppAccountNotFoundException | NoCurrentAccountSelectedException e) {
                Log.e(TAG, "NC account signIn issue", e);
            }
        }
    }

    private void passResultViaCallBack(boolean result, @Nullable EncryptionState state) {
        if (state != null) {
            String resultedContent = state.getResultedContent();
            if (resultedContent == null) {
                resultedContent = state.getProvidedContent();
            }

            state.getCallBack().contentReady(result, resultedContent);
            removeState(state);
        }
    }

    @Override
    public void startPendingIntentForResult(PendingIntent pendingIntent, int requestCode) throws IntentSender.SendIntentException {
        startIntentSenderFromChild(this, pendingIntent.getIntentSender(), requestCode, null, 0, 0, 0);
    }

    @Override
    public void setUpCompleted(EncryptionState state) {
        if (state != null && state.getType() == EncryptionState.Type.SET_UP) {
            passResultViaCallBack(true, state);
        }
    }

    @Override
    public void operationFailed(@Nullable String errorMessage, EncryptionState state) {
        if (state != null) {
            state.setResultedContent(state.getProvidedContent());
        }

        passResultViaCallBack(false, state);

        Toast.makeText(this, R.string.encryption_operation_failed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void operationCompleted(EncryptionState result) {
        passResultViaCallBack(true, result);
    }

    private Map<String, EncryptionState> getRunningEncryptions() {
        if (runningEncryptions == null) {
            runningEncryptions = new HashMap<>();
        }
        return runningEncryptions;
    }
}
