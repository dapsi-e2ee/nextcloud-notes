package it.niedermann.owncloud.notes.encryption;

public enum EncryptionStatus {

    READY,
    SETUP_REQUIRED
}
