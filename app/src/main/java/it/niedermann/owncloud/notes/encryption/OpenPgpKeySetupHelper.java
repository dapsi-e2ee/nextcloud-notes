package it.niedermann.owncloud.notes.encryption;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.util.Log;

import androidx.annotation.NonNull;

import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.util.OpenPgpApi;
import org.openintents.openpgp.util.OpenPgpServiceConnection;

public class OpenPgpKeySetupHelper {

    private static final String TAG = OpenPgpKeySetupHelper.class.getSimpleName();
    private static final int NO_KEY = 0;

    private final OpenPgpServiceConnection openPgpServiceConnection;
    private final String userId;
    private final Context context;
    private final OpenPgpKeySetupCallBack callBack;

    public OpenPgpKeySetupHelper(@NonNull Context context, @NonNull OpenPgpServiceConnection openPgpServiceConnection, @NonNull String userId, @NonNull OpenPgpKeySetupCallBack callBack) {
        this.context = context;
        this.openPgpServiceConnection = openPgpServiceConnection;
        this.userId = userId;
        this.callBack = callBack;
    }

    public void getSignKeyId(Intent data, EncryptionState state) {
        data.setAction(OpenPgpApi.ACTION_GET_SIGN_KEY_ID);
        data.putExtra(OpenPgpApi.EXTRA_USER_ID, userId);

        OpenPgpApi api = new OpenPgpApi(context, openPgpServiceConnection.getService());

        api.executeApiAsync(data, null, null, new OpenPgpCallback(state));
    }

    private class OpenPgpCallback implements OpenPgpApi.IOpenPgpCallback {

        private final EncryptionState state;

        private OpenPgpCallback(EncryptionState state) {
            this.state = state;
        }

        @Override
        public void onReturn(Intent result) {
            switch (result.getIntExtra(OpenPgpApi.RESULT_CODE, OpenPgpApi.RESULT_CODE_ERROR)) {
                case OpenPgpApi.RESULT_CODE_SUCCESS: {
                    long keyId = result.getLongExtra(OpenPgpApi.EXTRA_SIGN_KEY_ID, NO_KEY);
                    save(keyId, state);
                    break;
                }

                case OpenPgpApi.RESULT_CODE_USER_INTERACTION_REQUIRED: {
                    PendingIntent pi = result.getParcelableExtra(OpenPgpApi.RESULT_INTENT);
                    try {
                        state.setCurrentRequestCode(callBack.getNewRequestCode());
                        callBack.startPendingIntentForResult(pi, state.getCurrentRequestCode());
                    } catch (IntentSender.SendIntentException e) {
                        Log.e(TAG, "SendIntentException", e);
                        callBack.operationFailed(e.getMessage(), state);
                    }
                    break;
                }

                case OpenPgpApi.RESULT_CODE_ERROR: {
                    OpenPgpError error = result.getParcelableExtra(OpenPgpApi.RESULT_ERROR);
                    Log.e(TAG, "RESULT_CODE_ERROR: " + error.getMessage());
                    callBack.operationFailed(error.getMessage(), state);
                    break;
                }
            }
        }
    }

    private void save(long newValue, EncryptionState state) {
        callBack.saveKeyId(newValue, state);
    }


    public boolean handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        var state = callBack.getStateByRequestCode(requestCode);
        if (state != null && state.isOnKeySetupState()) {

            if (resultCode == Activity.RESULT_OK && data != null) {
                getSignKeyId(data, state);
            } else {
                callBack.operationFailed("openPgpKeySetupHelper handleActivityResult resultCode = " + resultCode + " data is null=" + (data==null), state);
            }
            return true;
        }

        return false;
    }
}
