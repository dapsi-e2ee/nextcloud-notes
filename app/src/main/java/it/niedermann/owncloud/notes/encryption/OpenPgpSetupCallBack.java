package it.niedermann.owncloud.notes.encryption;

import android.app.PendingIntent;
import android.content.IntentSender;

import androidx.annotation.Nullable;

public interface OpenPgpSetupCallBack {

    void startPendingIntentForResult(PendingIntent pendingIntent, int requestCode) throws IntentSender.SendIntentException;

    void setUpCompleted(EncryptionState state);

    void operationFailed(@Nullable String errorMessage, EncryptionState state);

    void operationCompleted(EncryptionState state);

    int getNewRequestCode();

    @Nullable
    EncryptionState getStateByRequestCode(int requestCode);
}
