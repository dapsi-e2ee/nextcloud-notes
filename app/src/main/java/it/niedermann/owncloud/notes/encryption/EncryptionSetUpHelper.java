package it.niedermann.owncloud.notes.encryption;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

interface EncryptionSetUpHelper {

    void onDestroy();

    boolean handleOnActivityResult(int requestCode, int resultCode, @Nullable Intent data);

    @NonNull
    EncryptionStatus getCurrentStatus();

    void operate(@NonNull EncryptionState state);

    void clearSetup();
}
