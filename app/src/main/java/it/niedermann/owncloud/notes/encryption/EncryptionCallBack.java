package it.niedermann.owncloud.notes.encryption;

public interface EncryptionCallBack {

    void contentReady(boolean success, String content);
}
