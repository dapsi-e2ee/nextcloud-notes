package it.niedermann.owncloud.notes.encryption;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;

import org.openintents.openpgp.IOpenPgpService2;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.util.OpenPgpApi;
import org.openintents.openpgp.util.OpenPgpServiceConnection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import it.niedermann.owncloud.notes.shared.util.SupportUtil;

class EncryptionSetUpHelperImpl implements EncryptionSetUpHelper {

    private static final String TAG = EncryptionSetUpHelperImpl.class.getSimpleName();

    private final Context context;
    private final String userEmail;
    private final OpenPgpSetupCallBack callBack;
    private final String preference_pgp_provider;
    private final String preference_pgp_key;
    private final SharedPreferences sharedPreferences;

    private OpenPgpServiceConnection openPgpServiceConnection;
    private OpenPgpKeySetupHelper pgpKeySetupHelper;
    private long pgpKey = 0L;

    EncryptionSetUpHelperImpl(@NonNull Context context, @NonNull String userEmail, @NonNull OpenPgpSetupCallBack callBack) {
        this.context = context;
        this.userEmail = userEmail;
        this.callBack = callBack;

        preference_pgp_provider = "PREFERENCE_PGP_PROVIDER_" + userEmail;
        preference_pgp_key = "PREFERENCE_PGP_KEY_" + userEmail;

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }

    @Override
    public void onDestroy() {
        if (openPgpServiceConnection != null) {
            openPgpServiceConnection.unbindFromService();
        }
    }

    @Override
    public boolean handleOnActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (pgpKeySetupHelper != null && pgpKeySetupHelper.handleOnActivityResult(requestCode, resultCode, data)) {
            return true;
        }

        EncryptionState state = callBack.getStateByRequestCode(requestCode);

        if (state != null && !state.isOnKeySetupState()) {

            if (resultCode == Activity.RESULT_OK && data != null) {

                switch (state.getType()) {
                    case ENCRYPT:
                        signAndEncrypt(data, state);
                        break;
                    case DECRYPT:
                        decryptAndVerify(data, state);
                        break;
                    default:
                        handleOperationFinished(null, state);
                }
            } else {
                setOperationFailed("EncryptionSetupHelper handleActivity resultCode: " + resultCode + " data is null=" + (data == null), state);
            }

            return true;
        }

        return false;
    }

    @Override
    @NonNull
    public EncryptionStatus getCurrentStatus() {
        String provider = sharedPreferences.getString(preference_pgp_provider, null);
        if (TextUtils.isEmpty(provider)) {
            return EncryptionStatus.SETUP_REQUIRED;
        }

        pgpKey = sharedPreferences.getLong(preference_pgp_key, 0L);
        if (pgpKey == 0L) {
            return EncryptionStatus.SETUP_REQUIRED;
        }

        return EncryptionStatus.READY;
    }

    @Override
    public void clearSetup() {
        sharedPreferences.edit()
                .remove(preference_pgp_provider)
                .remove(preference_pgp_key)
                .apply();
    }

    @Override
    public void operate(@NonNull EncryptionState state) {
        switch (state.getType()) {
            case ENCRYPT:
                encrypt(state);
                break;
            case DECRYPT:
                decrypt(state);
                break;
            case SET_UP:
                setup(state);
                break;
        }
    }

    private void setup(EncryptionState state) {
        if (isOpenPgpServiceInitialized()) {
            handleOperationFinished(null, state);
            return;
        }

        startSetup(state);
    }

    private void encrypt(EncryptionState state) {
        if (isOpenPgpServiceInitialized()) {
            signAndEncrypt(new Intent(), state);
            return;
        }

        startSetup(state);
    }

    private void decrypt(EncryptionState state) {
        if (isOpenPgpServiceInitialized()) {
            decryptAndVerify(new Intent(), state);
            return;
        }

        startSetup(state);
    }

    private void startSetup(EncryptionState state) {
        String provider = getProvider();

        if (TextUtils.isEmpty(provider)) {
            setOperationFailed("Failed to locate OpenPGP provider", state);
            return;
        }

        if (openPgpServiceConnection == null) {
            initiateOpenPGPSetup(provider, state);
        }
    }

    private boolean isOpenPgpServiceInitialized() {
        return openPgpServiceConnection != null && openPgpServiceConnection.isBound();
    }

    private void initiateOpenPGPSetup(String provider, EncryptionState state) {
        openPgpServiceConnection = new OpenPgpServiceConnection(context.getApplicationContext(), provider, new OpenPgpServiceConnection.OnBound() {
            @Override
            public void onBound(IOpenPgpService2 service) {
                Log.d(TAG, "openPGP bounded");
                if (getPgpKey() == 0L) {
                    initiatePGPKeySetup(state);
                    return;
                }

                onOpenPgpSetUpCompleted(state);
            }

            @Override
            public void onError(Exception e) {
                Log.e(TAG, "OpenPGP bound error", e);
                setOperationFailed(e.getMessage(), state);
            }
        });

        openPgpServiceConnection.bindToService();
    }

    private void onOpenPgpSetUpCompleted(EncryptionState state) {
        state.setOnKeySetupState(false);
        callBack.setUpCompleted(state);

        switch (state.getType()) {
            case ENCRYPT:
                encrypt(state);
                break;
            case DECRYPT:
                decrypt(state);
                break;
        }
    }

    private void initiatePGPKeySetup(EncryptionState state) {
        pgpKeySetupHelper = new OpenPgpKeySetupHelper(context, EncryptionSetUpHelperImpl.this.openPgpServiceConnection, getUserIdentity(), new OpenPgpKeySetupCallBack() {
            @Override
            public void startPendingIntentForResult(PendingIntent pendingIntent, int requestCode) throws IntentSender.SendIntentException {
                callBack.startPendingIntentForResult(pendingIntent, requestCode);
            }

            @Override
            public void saveKeyId(long key, EncryptionState encryptionState) {
                pgpKey = key;
                savePGPKey();
                onOpenPgpSetUpCompleted(encryptionState);
            }

            @Override
            public void operationFailed(String message, EncryptionState encryptionState) {
                state.setOnKeySetupState(false);
                setOperationFailed(message, encryptionState);
            }

            @Override
            public int getNewRequestCode() {
                return callBack.getNewRequestCode();
            }

            @Nullable
            @Override
            public EncryptionState getStateByRequestCode(int requestCode) {
                return callBack.getStateByRequestCode(requestCode);
            }
        });

        state.setOnKeySetupState(true);
        pgpKeySetupHelper.getSignKeyId(new Intent(), state);
    }

    private long getPgpKey() {
        pgpKey = sharedPreferences.getLong(preference_pgp_key, 0L);
        return pgpKey;
    }

    private void savePGPKey() {
        sharedPreferences.edit()
                .putLong(preference_pgp_key, pgpKey)
                .apply();
    }

    //FIXME: the format should be "name <email>"
    private String getUserIdentity() {
        return userEmail + " <" + userEmail + ">";
    }

    @Nullable
    private String getProvider() {
        String provider = sharedPreferences.getString(preference_pgp_provider, null);
        if (TextUtils.isEmpty(provider)) {
            provider = OpenPgpUtil.getOpenPgpProviderPackage(context);
            updateProviderValue(provider);
        }
        return provider;
    }

    private void updateProviderValue(String provider) {
        sharedPreferences.edit()
                .putString(preference_pgp_provider, provider)
                .apply();
    }

    private void signAndEncrypt(Intent data, EncryptionState state) {
        if (requirementMissing(state)) {
            return;
        }

        long[] selfEncryptIds = { pgpKey };

        data.setAction(OpenPgpApi.ACTION_ENCRYPT);
        data.putExtra(OpenPgpApi.EXTRA_KEY_IDS, selfEncryptIds);
        data.putExtra(OpenPgpApi.EXTRA_REQUEST_ASCII_ARMOR, true);


        executeEncryptionApiAsync(data, state);
    }

    private void decryptAndVerify(Intent data, EncryptionState state) {
        if (requirementMissing(state)) {
            setOperationFailed("Requirement missing", state);
            return;
        }

        if (TextUtils.isEmpty(state.getProvidedContent())) {
            setOperationFailed("Content is empty", state);
            return;
        }

        if (!SupportUtil.isEncryptedText(state.getProvidedContent())) {
            setOperationFailed("Provided Text is not encrypted", state);
            return;
        }

        data.setAction(OpenPgpApi.ACTION_DECRYPT_VERIFY);

        executeEncryptionApiAsync(data, state);
    }

    private boolean requirementMissing(EncryptionState state) {
        if (state.getProvidedContent() == null) {
            setOperationFailed("Invalid content", state);
            return true;
        }

        if (pgpKey == 0L) {
            setOperationFailed("OpenPGP key not found", state);
            return true;
        }
        return false;
    }

    private void setOperationFailed(String message, EncryptionState state) {
        callBack.operationFailed(message, state);
    }

    private InputStream getInputStream(String content) {
        return new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
    }

    private void executeEncryptionApiAsync(Intent data, EncryptionState state) {
        InputStream inputStream = getInputStream(state.getProvidedContent());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        OpenPgpApi api = new OpenPgpApi(context, openPgpServiceConnection.getService());

        api.executeApiAsync(data, inputStream, outputStream, result -> {
            switch (result.getIntExtra(OpenPgpApi.RESULT_CODE, OpenPgpApi.RESULT_CODE_ERROR)) {
                case OpenPgpApi.RESULT_CODE_SUCCESS: {
                    handleOpenPgpApiResultSuccess(outputStream, state);
                    break;
                }

                case OpenPgpApi.RESULT_CODE_USER_INTERACTION_REQUIRED: {
                    handleOpenPgpApiResultUIRequired(result, state);
                    break;
                }
                case OpenPgpApi.RESULT_CODE_ERROR: {
                    handleOpenPgiApiResultError(result, state);
                    break;
                }
            }
        });
    }

    private void handleOperationFinished(String result, EncryptionState state) {
        state.setResultedContent(result);
        callBack.operationCompleted(state);
    }

    private void handleOpenPgpApiResultSuccess(ByteArrayOutputStream outputStream, EncryptionState state) {
        try {
            handleOperationFinished(outputStream.toString("UTF-8"), state);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "encryption result conversion failed", e);
            setOperationFailed(e.getMessage(), state);
        }
    }

    private void handleOpenPgpApiResultUIRequired(Intent result, EncryptionState state) {
        PendingIntent pendingIntent = result.getParcelableExtra(OpenPgpApi.RESULT_INTENT);
        try {
            state.setCurrentRequestCode(callBack.getNewRequestCode());
            callBack.startPendingIntentForResult(pendingIntent, state.getCurrentRequestCode());
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "SendIntentException", e);
            setOperationFailed(e.getMessage(), state);
        }
    }

    private void handleOpenPgiApiResultError(Intent result, EncryptionState state) {
        OpenPgpError error = result.getParcelableExtra(OpenPgpApi.RESULT_ERROR);
        Log.e(TAG, "onError getErrorId:" + error.getErrorId());
        Log.e(TAG, "onError getMessage:" + error.getMessage());
        setOperationFailed(error.getMessage(), state);
    }
}
